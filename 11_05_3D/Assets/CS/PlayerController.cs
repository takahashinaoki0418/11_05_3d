using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");  //横方向の入力値（-1.0〜1.0）
        float z = Input.GetAxis("Vertical");    //縦方向の入力値（-1.0〜1.0）
        this.GetComponent<Rigidbody>().AddForce(x, 0.0f, 0.0f);
        this.GetComponent<Rigidbody>().AddForce(0.0f, 0.0f, z);
    }
}
