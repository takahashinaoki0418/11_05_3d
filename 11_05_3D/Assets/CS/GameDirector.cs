using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    int missCount = 0;
    public static int scoreCount = 0;
    public GameObject[] Hps;
    public GameObject score;
    public GameObject gameOverText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   public void HP()
    {
        missCount++;
            if (missCount == 5) 
        {
            SceneManager.LoadScene("GameOver");
        }
        Destroy(Hps[missCount-1]);

    }
    public void Score()
    {
        scoreCount++;
        score.GetComponent<Text>().text = ""+scoreCount;

    }
}
